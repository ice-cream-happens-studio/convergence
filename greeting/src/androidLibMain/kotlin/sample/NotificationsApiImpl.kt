package sample

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.provider.CalendarContract
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import org.greeting.R


const val RemindersTag = "RemindersTag"

class NotificationsApiImpl(private val application:Application) : NotificationsApi {

    override fun createLocalNotification(title:String, message:String, notificationId: Int, notificationTag: String) {
        val channelId = RemindersTag
        val builder = NotificationCompat.Builder(application, channelId)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setCategory(NotificationCompat.CATEGORY_REMINDER)

        NotificationManagerCompat.from(application).notify(notificationId,builder.build())

    }

    override fun cancelLocalNotification(notificationId: Int) {
        NotificationManagerCompat.from(application).cancel(notificationId)
    }


    override fun initializeNotifications() {
        createNotificationChannel()
    }

    override fun deinitializeNotifications() {
    }


    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Напоминания"
            val descriptionText = "Напоминания из DBTime"
            val channelId = RemindersTag
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, name, importance).apply {
                description = descriptionText
            }

            // Register the channel with the system
            val notificationManager: NotificationManager = application.getSystemService(Context
                    .NOTIFICATION_SERVICE) as NotificationManager


            if(!notificationManager.notificationChannels.contains(channel)) {
                notificationManager.createNotificationChannel(channel)
            }
        }
    }
}
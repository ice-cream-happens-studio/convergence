package sample


import android.app.Application
//import com.database.DbImpl
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import org.greeting.StringRepository
import AndroidStringRepository
import com.database.DbImpl
import emot.BehaviorAndSkillsRepository


private lateinit var application: Application
/**
 *  Call from [Application.onCreate] to do various
 *  initializations that rely on application context
 */
fun initLibrary(app: Application) {
    application = app
    driver
    Database
    with(BehaviorAndSkillsRepository(Database)) {
        getAllBehaviors().executeAsList()
                .ifEmpty {
                    insertBehaviors(
                            "Наркотики",
                            "Алкоголь",
                            "Самоповреждения",
                            "Суицид",
                            "Импульсивность"
                    )
                    insertSkills(mapOf(
                            "Осознанность" to listOf(
                                    "Мудрое сознание" to "TODO",
                                    "Наблюдение" to "What - see, hear, touch, smell, taste",
                                    "Описание" to "TODO",
                                    "Участие" to "TODO",
                                    "Безоценочность" to "Describe wihout blame",
                                    "Однозадачность" to "Concentrate on one task at a time",
                                    "Эффективность" to "TODO",
                                    "Осознанное питание" to "TODO",
                                    "Серфинг на волне желания" to "TODO"
                            ),
                            "Устойчивость к стрессу" to listOf(
                                    "Навыки STOP" to "Stop, Take deep breaths, Observe, Proceed effectively",
                                    "Навыки TIPP" to "Temperature, Intense physical sensations, Paced breathing, Paired muscle relax",
                                    "Навыки отвлечения ACCEPTS" to "Activities, Contributing, Comparisons, Emotion opposites, Pushing away, Thoughts, Self-soothe",
                                    "Навыки самоуспокоения – 5 чувств" to "TODO",
                                    "Навыки улучшения настоящего - IMPROVE" to "Image, Meaning, Prayer, Relaxation, One thing a a time, Vacation, Encouragement",
                                    "Радикальное принятие" to "TODO",
                                    "Полуулыбка, раскрытые ладони" to "TODO",
                                    "Готовность" to "TODO",
                                    "Осознанность мыслей" to "TODO"
                            ),
                            "Эмоциональная регуляция" to listOf(
                                    "Определение эмоций" to "Identify, label functions of emotion",
                                    "Проверка фактов" to "Проверь соотвествие эмоций фактам",
                                    "Противоположное действие" to "TODO",
                                    "Решение проблем" to "TODO",
                                    "А: накапливать позитивные эмоции" to "TODO",
                                    "В: наращивать мастерство" to "TODO",
                                    "С: справляться заранее" to "TODO",
                                    "PLEASE: снижать физическую уязвимость" to "Treat Physical iLlness, Eat healthy, Avoid drugs, Sleep healthy, Exercise",
                                    "Альтернативный бунт" to "TODO"
                            ),
                            "Межличностная эффективность" to listOf(
                                    "Навыки DEAR" to "Describe, Express, Assert, Reinforce",
                                    "Навыки MAN" to "Mindful, Appear confident, Negotiate",
                                    "Навыки GIVE" to "Gentle, Interested, Validate, Easy manner",
                                    "Навыки FAST" to " Fair, Apology free, Stick to value, Truthfulness",
                                    "Валидация" to "TODO"
                            )
                    ))
                }
    }
}


actual fun driver(): SqlDriver = driver.value

val notificationsApi: NotificationsApi by lazy { NotificationsApiImpl(application) }

actual val Strings: StringRepository by lazy { AndroidStringRepository(application) }
val driver = lazy { AndroidSqliteDriver(DbImpl.Schema, application.applicationContext, "app.db") }
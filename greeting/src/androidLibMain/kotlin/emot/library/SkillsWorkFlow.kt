package library

import com.squareup.workflow.RenderContext
import com.squareup.workflow.Snapshot
import com.squareup.workflow.StatefulWorkflow
import emot.BehaviorAndSkillsRepository
import emot.Skill
import com.squareup.workflow.asWorker
import com.squareup.workflow.*
import library.AppWorkflow.ViewState.Skills.Page
import sample.*


class SkillsWorkFlow(private val behaviorAndSkillsRepository: BehaviorAndSkillsRepository) :
        StatefulWorkflow<Unit, SkillsWorkFlow.State, Nothing, AppWorkflow.ViewState.Skills>() {

    data class State(
            val page: Page = Page.Root,
            val skills: List<Skill> = emptyList()
    )

    override fun initialState(props: Unit, snapshot: Snapshot?): SkillsWorkFlow.State = State()


    override fun render(props: Unit, state: SkillsWorkFlow.State, context: RenderContext<SkillsWorkFlow.State, Nothing>): AppWorkflow.ViewState.Skills {
        context.runningWorker(behaviorAndSkillsRepository.getAllSkills().asFlow().mapToList().asWorker()) {
            action {
                val sk = it.map { Skill(it.id, it.name, it._group_, it.description) }
                nextState = nextState.copy(skills =sk)
            }
        }

        val grouped = state.skills.groupBy { it.group }.toList()
        return AppWorkflow.ViewState.Skills(state.page, grouped,{context.onPageChange(it)},when(state.page) {
            Page.Root -> emptyList<()->Unit>()
            is Page.Group -> listOf<()->Unit> {
                context.actionSink.send(action {
                    nextState = nextState.copy(page = Page.Root)
                })
            }
        }.let { it })
    }


    fun RenderContext<SkillsWorkFlow.State, Nothing>.onPageChange(page: String):Unit {
        actionSink.send(action {
            nextState = nextState.copy(page = Page.Group(page))
        })
    }


    override fun snapshotState(state: SkillsWorkFlow.State): Snapshot = Snapshot.EMPTY

}

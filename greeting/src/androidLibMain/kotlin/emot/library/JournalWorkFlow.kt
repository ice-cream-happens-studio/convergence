package library

import com.soywiz.klock.DateTimeTz
import com.squareup.workflow.RenderContext
import com.squareup.workflow.Snapshot
import com.squareup.workflow.StatefulWorkflow
import com.squareup.workflow.asWorker
import emot.*
import kotlinx.coroutines.flow.*
import com.squareup.workflow.*
import kotlinx.serialization.UnionKind
import sample.schema.SelectBehavior
import sample.schema.SelectSkills

data class JournalEntry(
    val entry: JournalEntryData,
    val skills: List<SkillEntry>,
    val behavior: List<Behavior>
)


sealed class JournalOutput {
    class EditEntry(val entry: Pair<DateTimeTz, JournalEntry>) : JournalOutput()
    object NewEntry : JournalOutput()
}

class JournalWorkFlow(private val journalRepository: JournalRepository) :
    StatefulWorkflow<Unit, JournalWorkFlow.State, JournalOutput, AppWorkflow.ViewState.Journal>() {

    //TODO: Serializable
    data class State(
        val list: List<Pair<DateTimeTz, JournalEntry>>,
        val full: Boolean = false
    )


    override fun initialState(
        props: Unit,
        snapshot: Snapshot?
    ): State = State(emptyList())

    override fun render(
        props: Unit,
        state: State,
        context: RenderContext<State, JournalOutput>
    ): AppWorkflow.ViewState.Journal {

        context.runningWorker(with(journalRepository) {
            selectBe().zip(selectSk()) { a, b -> a to b }.zip(selectJournal()) { f, s ->
                val a = f.first
                val b = f.second

                val beh = a.groupBy(SelectBehavior::time).mapValues {
                    it.value.map {
                        Behavior(
                            it.id,
                            it.name,
                            (it.desought.toFloat()),
                            (it.action.toFloat())
                        )
                    }
                }
                val skil = b.groupBy(SelectSkills::time).mapValues {
                    it.value.map {
                        SkillEntry(
                            it.id,
                            it.name,
                            it._group_,
                            it.description,
                            it.score.toFloat()
                        )
                    }
                }

                s.map {
                    JournalEntryData(
                        it.physical_discomfort.toFloat(),
                        it.emotional_discomfort.toFloat(),
                        it.happiness.toFloat(),
                        it.discontinuation.toFloat(),
                        comment = ""
                    ) to it.time
                }.map {
                    DateTimeTz.fromUnixLocal(it.second) to JournalEntry(
                        it.first,
                        skil[it.second].orEmpty(),
                        beh[it.second].orEmpty()
                    )
                }.asReversed()

            }
        }.asWorker()) {
            action {
                nextState = nextState.copy(list = it)
            }
        }


        return AppWorkflow.ViewState.Journal(listOf {
            context.actionSink.send(action { nextState = nextState.copy(full = state.full.not()) }

            )
        }, state.list.let { list ->
            if (state.full.not()) {
                list.map {
                    it.copy(
                        second =
                        it.second.copy(
                            behavior = it.second.behavior.filter { it.actionLevel != 0F || it.desireLevel != 0F },
                            skills = it.second.skills.filter { it.score != 0F || it.comment.isNotEmpty() })
                    )
                }
            } else {
                list
            }
        }, { date ->
            context.actionSink.send(action {
                setOutput(JournalOutput.EditEntry(state.list.first { it.first == date }))
            })
        }, {
            context.actionSink.send(action {
                setOutput(JournalOutput.NewEntry)
            })
        })
    }

    override fun snapshotState(state: State): Snapshot = Snapshot.EMPTY
}
package org.konan.multiplatform.library

import com.soywiz.klock.DateTimeTz
import com.squareup.workflow.*
import library.AppWorkflow
import sample.asFlow
import sample.mapToList
import emot.Behavior
import emot.SkillEntry
import emot.BehaviorAndSkillsRepository
import emot.JournalEntryData
import emot.JournalRepository
import kotlinx.serialization.*
import library.DateSerializer
import library.JournalEntry


class EntryWorkFlow(private val journalRepository: JournalRepository,
                    private val behaviorAndSkillsRepository: BehaviorAndSkillsRepository) :
        StatefulWorkflow<Pair<DateTimeTz,JournalEntry>?, EntryWorkFlow.State, Unit, AppWorkflow.ViewState.Entry>() {

    @Serializable
    data class State(
            val behaviors: List<Behavior> = emptyList(),
            val skills: List<SkillEntry> = emptyList(),
            val _behaviors: List<Behavior> = emptyList(),
            val _skills: List<SkillEntry> = emptyList(),
            val general: JournalEntryData = JournalEntryData(comment = ""),
            @Serializable(with = DateSerializer::class)
            val date: DateTimeTz? = null,
            val page: Int = 0
    )



    @ImplicitReflectionSerializer
    override fun initialState(
            props: Pair<DateTimeTz,JournalEntry>?,
            snapshot: Snapshot?
    ): State = props?.let { State( behaviors = props.second.behavior,
            skills = props.second.skills,
            general = props.second.entry,
            date = props.first) } ?: State()

    override fun onPropsChanged(old: Pair<DateTimeTz, JournalEntry>?, new: Pair<DateTimeTz, JournalEntry>?, state: State): State =
            new?.let { State( behaviors = new.second.behavior,
                    skills = new.second.skills,
                    _behaviors = state._behaviors,
                    _skills = state._skills,
                    general = new.second.entry,
                    date = new.first,
                    page = 0) } ?: State(state._behaviors,state._skills)

    override fun render(
            props: Pair<DateTimeTz,JournalEntry>?,
            state: State,
            context: RenderContext<State, Unit>
    ): AppWorkflow.ViewState.Entry {



            context.runningWorker(behaviorAndSkillsRepository.getAllBehaviors().asFlow().mapToList().asWorker(), "") {
                action {
                    val behav = it.map {beh->
                        state.behaviors.firstOrNull() { it.id == beh.id }?.let { Behavior(beh.id, beh.name,it.desireLevel,it.actionLevel,it.comment)  }
                                ?:Behavior(beh.id, beh.name)
                    }
                    nextState = nextState.copy(behaviors = behav,_behaviors = it.map { Behavior(it.id,it.name) })
                }
            }


            context.runningWorker(behaviorAndSkillsRepository.getAllSkills().asFlow().mapToList().asWorker()) {
                action {
                    val skills = it.map {  skill ->
                        state.skills.firstOrNull() {it.id ==skill.id}?.let { SkillEntry(skill.id, skill.name, skill._group_,skill.description,it.score,it.comment) }
                                ?: SkillEntry(skill.id, skill.name, skill._group_,skill.description) }
                    nextState = nextState.copy(skills = skills, _skills = it.map { SkillEntry(it.id,it.name,it._group_,it.description) })
                }
            }


        val entries = when(state.page) {
            0 -> listOf(state.general)
            1 -> state.behaviors
            2 -> state.skills
            else -> {
                emptyList()
            }
        }
        return AppWorkflow.ViewState.Entry(entries, state.page,
                {
                    context.actionSink.send(action {
                        nextState = nextState.copy(page = nextState.page+1)
                    })
                },
                {
                    context.actionSink.send(action {
                        nextState = nextState.copy(page = nextState.page-1)
                    })
                },
                { entry ->
                    context.actionSink.send(action {
                        nextState = when (entry) {
                            is Behavior -> {
                                val set = nextState.behaviors.toMutableList()
                                val ind = set.indexOfFirst { it.id == entry.id }
                                set[ind] = entry
                                nextState.copy(behaviors = set)
                            }
                            is SkillEntry -> {
                                val set = nextState.skills.toMutableList()
                                val ind = set.indexOfFirst { it.id == entry.id }
                                set[ind] = entry
                                nextState.copy(skills = set)
                            }
                            is JournalEntryData -> {
                                nextState.copy(general = entry)
                            }
                        }
                    })
                })
        {
            context.actionSink.send(action {
                journalRepository.insertRecord(state.general,state.behaviors,state.skills,state.date?: DateTimeTz.nowLocal())
                nextState = State(state._behaviors,state._skills,general = JournalEntryData())
                setOutput(Unit)
            })

        }
    }





    override fun snapshotState(state: State): Snapshot = Snapshot.EMPTY//Snapshot.of(ByteString.of(*Cbor().dump(State.serializer(),state)))
}


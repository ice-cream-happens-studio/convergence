/*
package library

import androidx.compose.isMainThread
import androidx.ui.foundation.isSystemInDarkTheme
import com.soywiz.klock.DateTimeTz
import com.squareup.workflow.RenderContext
import com.squareup.workflow.Snapshot
import com.squareup.workflow.StatefulWorkflow
import com.squareup.workflow.asWorker
import emot.JournalRepository
import emot.Reminder
import emot.RemindersRepository
import org.konan.multiplatform.library.EntryWorkFlow
import kotlin.reflect.KParameter
import kotlin.reflect.KProperty

data class ReminderModel(
        val reminder: Reminder,
        val editing: Boolean,
        val onEdit: (ReminderModel) -> Unit,
        val onSave: (Reminder) -> Unit)

class RemindersWorkFlow(private val remindersRepository: RemindersRepository) :
        StatefulWorkflow<Nothing, RemindersWorkFlow.State, Unit, AppWorkflow.ViewState.Reminders>() {

    data class State(
            val reminders: List<ReminderModel> = emptyList()
    )

    override fun initialState(props: Nothing, snapshot: Snapshot?): State = State()

    override fun snapshotState(state: State): Snapshot = Snapshot.EMPTY

    override fun render(props: Nothing, state: State, context: RenderContext<State, Unit>): AppWorkflow.ViewState.Reminders {
        context.runningWorker(remindersRepository.getReminders().asWorker()) {
            action {

            }
        }
        context.runningWorker(remindersRepository.getReminders().asWorker()) {
            action {
                nextState = state.copy(reminders = it.mapIndexed { index, reminder ->
                    ReminderModel(reminder, false, {
                        context.actionSink.send(action {
                            nextState = state.copy(state.reminders.toMutableList().apply {
                                removeAt(index)
                                add(it)
                            })
                        })
                    }, onSave = remindersRepository::insertReminder
                    )
                })
            }
        }

            */
/*val models = state.reminders.map { rem ->
                ReminderModel(rem,

                        ) {

                }
            }*//*

            return AppWorkflow.ViewState.Reminders(state.reminders) {
                context.actionSink.send(action {
                    nextState = state.copy(reminders = listOf(ReminderModel(Reminder(),true,
                            {
                                context.actionSink.send(action {
                                    nextState = state.copy(state.reminders.toMutableList()
                                            .removeAt(index)
                                            .add(it))
                                })
                            }, onSave = remindersRepository::insertReminder
                    )) + state.reminders)
                })
            }



    }
}

        */
/*inline fun <reified T : Any, reified K : Any> T.copy(kParameter: KProperty<K>, value: K): T {
            val copy = T::class.members.first { it.name == "copy" }//.memberFunctions.first { it.name == "copy" }
            val instanceParam = copy.parameters.first()
            val mod = copy.parameters.first { it.name == kParameter.name }
            return copy.callBy(mapOf(instanceParam to this, mod to value)) as T
        }*//*

*/
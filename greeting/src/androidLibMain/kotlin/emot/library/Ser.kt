package library

import com.soywiz.klock.DateTimeTz
import kotlinx.serialization.*
import kotlinx.serialization.internal.StringDescriptor

@Serializer(forClass = DateTimeTz::class)
object DateSerializer: KSerializer<DateTimeTz> {

    override val descriptor: SerialDescriptor =
            PrimitiveDescriptor("DateTimeTz", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, obj: DateTimeTz) {
        encoder.encodeDouble(obj.local.unixMillis)
    }

    override fun deserialize(decoder: Decoder): DateTimeTz {
        return DateTimeTz.fromUnixLocal((decoder.decodeDouble()))
    }
}
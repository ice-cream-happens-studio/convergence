package library

import com.squareup.workflow.RenderContext
import com.squareup.workflow.Snapshot
import com.squareup.workflow.StatefulWorkflow


data class BreateState(val unit: Unit=Unit)

class BreatheWorkFlow : StatefulWorkflow<Unit, BreateState, Nothing, AppWorkflow.ViewState.Breathe>() {
    override fun initialState(props: Unit, snapshot: Snapshot?) = BreateState()

    override fun render(props: Unit, state: BreateState, context: RenderContext<BreateState, Nothing>): AppWorkflow.ViewState.Breathe {
        return AppWorkflow.ViewState.Breathe(Unit)
    }

    override fun snapshotState(state: BreateState): Snapshot = Snapshot.EMPTY


}
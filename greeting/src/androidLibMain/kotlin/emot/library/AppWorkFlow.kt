package library

import android.graphics.pdf.PdfRenderer
import com.soywiz.klock.DateTimeTz
import com.soywiz.klock.TimeSpan
import com.squareup.workflow.*
import emot.Reminder
import emot.RemindersRepository
import emot.SkillEntry
import library.AppWorkflow.Rendering
import org.konan.multiplatform.library.EntryWorkFlow
import emot.Skill
import kotlinx.coroutines.flow.*
import sample.schema._skill


data class AppState(
    val screenState: ScreenState,
    val entry: Pair<DateTimeTz, JournalEntry>? = null,//Prop transfer//TODO: move to screenstate,
    val lightTheme: Boolean
) {


    sealed class ScreenState {
        class Journal() : ScreenState()
        class NewEntry() : ScreenState()
        class Reminders() : ScreenState()
        class Skills() : ScreenState()
        class Breathe() : ScreenState()
    }
}

//typealias Rendering = ViewState


class AppWorkflow(
    val entryWorkFlow: EntryWorkFlow,
    val journalWorkFlow: JournalWorkFlow,
    val remindersWorkFlow: RemindersWorkFlow,
    val skillsWorkFlow: SkillsWorkFlow,
    val breatheWorkFlow: BreatheWorkFlow
) : StatefulWorkflow<Unit, AppState, Nothing, Rendering>() {
    sealed class ViewState {
        open val actions = listOf<Any>()
        open val actionsLeft = listOf<Any>()

        data class Journal(
            override val actions: List<() -> Unit>,
            val list: List<Pair<DateTimeTz, JournalEntry>>,
            val onClick: (DateTimeTz) -> Unit,
            val onNewEntry: () -> Unit
        ) : ViewState()

        object EntryEditor : ViewState()

        data class Reminders(val list: List<ReminderModel>, val onNewEntry: () -> Unit) :
            ViewState()

        object Graph : ViewState()
        data class Entry(
            val list: List<emot.Entry>,
            val page: Int,
            val onNextPage: () -> Unit,
            val onPreviousPage: () -> Unit,
            val onEntryEdited: (emot.Entry) -> Unit,
            val onSave: () -> Unit
        ) : ViewState()

        data class Skills(
            val page: Page,
            val list: List<Pair<String, List<Skill>>>,
            val onPageChange: (String) -> Unit,
            override val actionsLeft: List<() -> Unit>
        ) : ViewState() {
            sealed class Page {
                object Root : Page()
                class Group(val group: String) : Page()
            }
        }

        data class Breathe(val unit: Unit):ViewState()
    }

    data class Rendering(
        val onThemeToggle: () -> Unit,
        val lightTheme: Boolean,
        val header: String,
        val screen: ViewState,
        val bottomBar: BottomBar
    )

    data class BottomBar(
        val onSelected: (Int) -> Unit,
        val selected: Int
    )

    override fun initialState(
        props: Unit,
        snapshot: Snapshot?
    ): AppState = AppState(screenState = AppState.ScreenState.Skills(), lightTheme = true)


    override fun render(
        props: Unit,
        state: AppState,
        context: RenderContext<AppState, Nothing>
    ): Rendering {


        val entry = context.renderChild(entryWorkFlow, props = state.entry) {
            action {
                nextState = nextState.copy(AppState.ScreenState.Journal(), entry = null)
            }
        }

        val journal = context.renderChild(journalWorkFlow, props = Unit) {
            action {
                nextState = nextState.copy(
                    AppState.ScreenState.NewEntry(), entry = when (it) {
                        is JournalOutput.EditEntry -> it.entry
                        is JournalOutput.NewEntry -> null
                    }
                )
            }
        }


        val breathe = context.renderChild(breatheWorkFlow, props = Unit)


        val reminders =
            context.renderChild(remindersWorkFlow, props = Unit)

        val skill = context.renderChild(skillsWorkFlow, props = Unit)

        val child =
            when (state.screenState) {
                is AppState.ScreenState.NewEntry -> entry
                is AppState.ScreenState.Journal -> journal
                is AppState.ScreenState.Reminders -> reminders
                is AppState.ScreenState.Skills -> skill
                is AppState.ScreenState.Breathe -> breathe
            }



        return Rendering({
            context.actionSink.send(com.squareup.workflow.action {
                nextState = nextState.copy(lightTheme = nextState.lightTheme.not())
            })
        }, state.lightTheme,
            when (state.screenState) {
                is AppState.ScreenState.Journal -> "Журнал"
                is AppState.ScreenState.NewEntry -> state.entry?.let { "Редактирование" }
                    ?: "Новая запись"
                is AppState.ScreenState.Reminders -> "Напоминания"
                is AppState.ScreenState.Skills -> "Навыки"
                is AppState.ScreenState.Breathe -> "Дыхание"
            }, child, BottomBar(
                {
                    context.actionSink.send(changeTab(it))
                }, when (state.screenState) {
                    is AppState.ScreenState.Reminders -> 2
                    is AppState.ScreenState.Journal -> 1
                    is AppState.ScreenState.NewEntry -> 1
                    is AppState.ScreenState.Skills -> 0
                    is AppState.ScreenState.Breathe -> 3
                }
            )
        )
    }

    override fun snapshotState(state: AppState): Snapshot = Snapshot.EMPTY

    private fun changeTab(tab: Int) = action {
        val tab = when (tab) {
            0 -> AppState.ScreenState.Skills()
            1 -> AppState.ScreenState.Journal()
            2 -> AppState.ScreenState.Reminders()
            3 -> AppState.ScreenState.Breathe()
            else -> return@action
        }
        nextState = nextState.copy(screenState = tab)
    }

}

/*
class JournalWorkflow(val journalRepository: JournalRepository) : StatefulWorkflow<Unit, JournalState, JournalEvents, ViewState.Journal>() {
    override fun initialState(props: Unit, snapshot: Snapshot?) = JournalState()



    override fun snapshotState(state: JournalState): Snapshot = Snapshot.EMPTY



    private fun JournalState.onEntries(list: List<JournalEntry>) = action {
        nextState = copy(entries = list)
    }

    override fun render(props: Unit, state: JournalState, context: RenderContext<JournalState, JournalEvents>): ViewState.Journal {

        context.runningWorker(journalRepository.getAllJournalEntries().asWorker(), handler = {state.onEntries(it)})

        val sink = context.makeActionSink<WorkflowAction<JournalState,JournalEvents>>()

        val onNewEntry = { sink.send(action{})}


        return shortCalendar(state,onNewEntry)
    }


    private fun shortCalendar(state: JournalState, onNewEntry: () -> Unit): ViewState.Journal {
        val now = DateTimeTz.nowLocal()


        val selectedDateOffset = state.selectedDate.dayOfYear%5
        val selected = state.selectedDate.dayOfYear
        val start = (selected-selectedDateOffset-4)
        val end = selected-selectedDateOffset

        val dates = start..end

        //if(state.selectedDate + DateTimeSpan(days = 5)>now)

        return with(DateTime.now()) {


            val dates = (start..end).map {
                (DateTime.createAdjusted(state.selectedDate.yearInt, state.selectedDate.month1, it))
            }


            val calendar = dates.map {
                toDayViewModel(it.dayOfMonth, state.dailyGoal, state
                        .entries, it
                        .month1)
            }
            val list = dates.map {
                toDayListViewModel(it.dayOfMonth, it.month1, state.entries)
            }.filter { it.entries.isNotEmpty() }


            ViewState.Journal
        }
    }



    private fun DateTime.toDayListViewModel(day: Int, month: Int, grouped: List<JournalEntry>): EmotionViewModel.DayListViewModel {

        val date = DateTime.createAdjusted(yearInt, month, day)
        val dateTo = DateTime.createAdjusted(yearInt, month, day + 1)
        val list = grouped
                .filter { date.unixMillisLong <= it.time.date && it.time.date < dateTo.unixMillisLong }
                .map {
                    EmotionViewModel.DayListViewModel.Entry(
                            DateTime.fromUnix(it.time.date).format("HH:mm"),
                            it.emotions.map {
                                EmotionViewModel.Emotion(it.name, it.color, it.color, it.id, OnAction{})
                            },
                            unixMillisLong)
                }

        return EmotionViewModel.DayListViewModel(day, list)
    }

    private fun DateTime.toDayViewModel(day: Int, dailyGoal: Int, grouped: List<JournalEntry>, month: Int): DayViewModel {
        val date = DateTime.createAdjusted(yearInt, month, day).plus(TimeSpan(1.0))
        val dateTo = DateTime.createAdjusted(yearInt, month, day + 1).minus(TimeSpan(1.0))
        val percentageOfGoal = ((grouped.filter { date.unixMillisLong <= it.time.date && it.time.date < dateTo.unixMillisLong }
                .size.toFloat() / dailyGoal)
                .coerceAtMost(1F) * 100)
                .toInt()

        return DayViewModel(day, percentageOfGoal)
    }

}
*/





data class ReminderModel(
    val reminder: Reminder,
    val editing: Boolean,
    val onEdit: (ReminderModel) -> Unit,
    val onSave: (ReminderModel) -> Unit,
    val onDelete: () -> Unit
)

class RemindersWorkFlow(private val remindersRepository: RemindersRepository) :
    StatefulWorkflow<Unit, RemindersWorkFlow.State, Nothing, AppWorkflow.ViewState.Reminders>() {

    data class State(
        val reminders: List<ReminderModel> = emptyList()
    )

    override fun initialState(props: Unit, snapshot: Snapshot?): State = State()

    override fun snapshotState(state: State): Snapshot = Snapshot.EMPTY

    override fun render(
        props: Unit,
        state: State,
        context: RenderContext<State, Nothing>
    ): AppWorkflow.ViewState.Reminders {

        fun onEdit(index: Int, reminder: ReminderModel) = run {
            context.actionSink.send(action {
                nextState = nextState.copy(nextState.reminders.toMutableList().apply {
                    removeAt(index)
                    add(index, reminder)
                })
            })
        }

        context.runningWorker(remindersRepository.getReminders().asWorker()) {
            action {
                nextState = nextState.copy(reminders = it.mapIndexed { index, reminder ->
                    val curr = nextState.reminders.getOrNull(index)
                    if (curr?.let { it.editing && it.reminder.id == reminder.id } == true) {
                        curr
                    } else
                        ReminderModel(
                            reminder,
                            false,
                            onEdit = { onEdit(index, it) },
                            onSave = { save(it) },
                            onDelete = { remindersRepository.deleteReminder(reminder.id!!) })
                })
            }
        }


        return AppWorkflow.ViewState.Reminders(state.reminders) {
            context.actionSink.send(action {

                nextState = nextState.copy(reminders = listOf(ReminderModel(Reminder(
                    id = null,
                    time = TimeSpan(0.0),
                    text = ""
                ), editing = true,
                    onEdit = { onEdit(0, it) },
                    onSave = { save(it) },
                    onDelete = {
                        context.actionSink.send(action {
                            nextState = nextState.copy(
                                reminders = nextState.reminders.toMutableList()
                                    .apply { removeAt(0) })
                        })
                    }
                )) + nextState.reminders)
            })
        }
    }


    val save = { it: ReminderModel -> it.reminder.let(remindersRepository::insertReminder) }
}

/*inline fun <reified T : Any, reified K : Any> T.copy(kParameter: KProperty<K>, value: K): T {
    val copy = T::class.members.first { it.name == "copy" }//.memberFunctions.first { it.name == "copy" }
    val instanceParam = copy.parameters.first()
    val mod = copy.parameters.first { it.name == kParameter.name }
    return copy.callBy(mapOf(instanceParam to this, mod to value)) as T
}*/

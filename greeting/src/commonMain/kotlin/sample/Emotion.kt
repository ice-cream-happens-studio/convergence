package data

data class Color(val hex: String)

data class Emotion(val id: Long, val name: String, val category: Category, val color: Color) {
    enum class Category(val color: Color) {
        //TODO: localized names
        Positive(
                Color("#228B22")
        ),
        Negative(Color("#DC143C")),
        Neutral(Color("#E6E6FA"))
    }
}


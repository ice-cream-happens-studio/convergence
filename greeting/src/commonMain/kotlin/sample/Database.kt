package sample

import com.database.DbImpl
import com.squareup.sqldelight.db.SqlDriver
import org.greeting.StringRepository

expect fun driver(): SqlDriver
fun createQueryWrapper(driver: SqlDriver) = DbImpl(driver = driver)
val Database by lazy { createQueryWrapper(driver()) }

object Schema : SqlDriver.Schema by DbImpl.Schema

expect val Strings: StringRepository
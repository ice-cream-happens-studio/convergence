package sample


interface NotificationsApi {

    fun createLocalNotification(title:String, message:String, notificationId: Int, notificationTag: String)

    fun cancelLocalNotification(notificationId: Int)

    fun initializeNotifications()

    fun deinitializeNotifications()

}
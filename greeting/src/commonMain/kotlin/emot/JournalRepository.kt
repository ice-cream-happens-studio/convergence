package emot


import com.database.DbImpl
import com.soywiz.klock.DateTimeTz
import com.soywiz.klock.TimeSpan
import kotlinx.serialization.Serializable
import sample.*


class JournalRepository(private val db: DbImpl) {
    fun insertRecord(general: JournalEntryData, behaviors: List<Behavior>, skills: List<SkillEntry>, timeTz: DateTimeTz) {
        val time = timeTz.local.unixMillisLong
        db.journalQueries.insertJournalEntity(time)
        db.journalQueries.insertJournalGeneralRecord(time, (general.emotionalDiscomfort.toDouble()), (general.physicalDiscomfort.toDouble()),(general.happiness.toDouble()),(general.therapyDiscontinuation.toDouble()))

        behaviors.forEach {
            db.journalQueries.insertJournalBehaviorRecord(time, it.id, (it.actionLevel.toDouble()), (it.desireLevel.toDouble()))
        }

        skills.forEach {
            db.journalQueries.insertJournalSkillRecord(time, it.id, (it.score.toDouble()))
        }
    }

    fun selectJournal() = db.someQueries.selectGeneral().asFlow().mapToList()
    fun selectBe() = db.someQueries.selectBehavior().asFlow().mapToList()
    fun selectSk() = db.someQueries.selectSkills().asFlow().mapToList()
}

class BehaviorAndSkillsRepository(private val db: DbImpl) {

    fun getAllBehaviors() = db.behaviorQueries.Behavior()

    fun getAllSkills() = db.behaviorQueries.Skill()

    fun insertBehaviors(vararg names: String) {
        db.transaction {
            names.forEach {
                db.behaviorQueries.InsertBehavior(it)
            }
        }
    }

    fun insertSkills(skills: Map<String, List<Pair<String,String>>>) {
        db.transaction {
            skills.entries.forEach { entry ->
                entry.value.forEach { (name,description) ->
                    db.behaviorQueries.InsertSkill(name, entry.key, description)
                }
            }
        }
    }

}


data class Reminder(val id: Long?, val time: TimeSpan, val text: String)
class RemindersRepository(private val db: DbImpl) {
    fun insertReminder(reminder: Reminder) {
        db.remindersQueries.insertReminders(reminder.id, reminder.time.millisecondsLong,reminder.text)
    }

    fun deleteReminder(id: Long) {
        db.remindersQueries.deleteReminder(id)
    }

    fun getReminders() = db.remindersQueries.selectReminders { id, time, text -> Reminder(id,TimeSpan(time.toDouble()),text) }.asFlow().mapToList()

}


sealed class Entry
sealed class TwoPointEntry : Entry()

fun TwoPointEntry.name(): String = when (this) {
    is Behavior -> name
    is JournalEntryData -> "Боль"
}

fun TwoPointEntry.subtitle(): Pair<String, String> = when (this) {
    is Behavior -> "Желание" to "Поведение"
    is JournalEntryData -> "Эмоциональная" to "Физическая"
}

fun SkillEntry.subtitle(): String = "Применение"

@Serializable
data class Behavior(val id: Long, val name: String, val desireLevel: Float = 0F, val actionLevel: Float = 0F, val comment: String = "") : TwoPointEntry()

@Serializable
data class SkillEntry(private val skill:Skill, val score: Float = 0F, val comment: String = "") : Entry(),ISkill by skill

fun SkillEntry(id: Long,  name: String,  group: String, description: String, score: Float = 0F, comment: String = "") = SkillEntry(Skill(id,name,group,description),score,comment)

interface ISkill {
    val id: Long
    val name: String
    val group: String
    val description: String
}

@Serializable
data class Skill(override val id: Long, override val name: String, override val group: String, override val description: String) : ISkill

@Serializable
data class JournalEntryData(val physicalDiscomfort: Float = 0F,
                            val emotionalDiscomfort: Float = 0F,
                            val happiness: Float = 0F,
                            val therapyDiscontinuation: Float = 0F,
                            val comment: String = "") : TwoPointEntry()
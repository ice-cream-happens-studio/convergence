package screens

import androidx.compose.Composable
import androidx.ui.core.Text
import androidx.ui.foundation.Clickable
import androidx.ui.foundation.VerticalScroller
import androidx.ui.layout.*
import androidx.ui.material.ripple.Ripple
import androidx.ui.text.TextStyle
import androidx.ui.text.style.TextAlign
import androidx.ui.unit.dp
import library.AppWorkflow
import org.konan.multiplatform.views.PaddedCard

@Composable
fun SkillsScreen(scr: AppWorkflow.ViewState.Skills) {
    Stack(modifier = LayoutSize.Fill) {
        VerticalScroller {
            Column {
                when(scr.page) {
                    AppWorkflow.ViewState.Skills.Page.Root -> scr.list.forEachIndexed { i, item ->
                        if (i % 2 == 0)
                            Row {
                                card(item.first) {
                                    scr.onPageChange(item.first)
                                }
                                scr.list.getOrNull(i + 1)?.first?.let {
                                    card(it) {
                                        scr.onPageChange(it)
                                    }
                                }
                            }
                    }
                    is AppWorkflow.ViewState.Skills.Page.Group -> scr.list.first{it.first== (scr.page as AppWorkflow.ViewState.Skills.Page.Group).group}.let {

                        PaddedCard(modifier = LayoutWidth.Fill + LayoutHeight(40.dp)) {
                            Text(style = TextStyle(textAlign = TextAlign.Center), modifier = LayoutAlign.Center, text = it.first)
                        }

                        it.second.forEachIndexed { i, item ->
                            if (i % 2 == 0)
                                Row {
                                    card(item.name + "\n\n" + item.description)
                                    it.second.getOrNull(i + 1)?.let {
                                        card(it.name + "\n\n" + it.description)
                                    }
                                }
                        }
                    }
                }.let {  }
            }
        }
    }
}

@Composable
private fun RowScope.card(item: String,onClick: ()->Unit = {}) {
    Ripple(bounded = true) {
        Clickable(onClick = onClick) {
            PaddedCard(modifier = LayoutWeight(1F) + LayoutHeight(150.dp)) {
                Text(style = TextStyle(textAlign = TextAlign.Center), modifier = LayoutAlign.Center, text = item)
            }
        }
    }
}
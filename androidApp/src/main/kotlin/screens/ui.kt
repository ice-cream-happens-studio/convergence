package screens


import android.app.Application
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.Model
import androidx.ui.animation.Crossfade
import androidx.ui.core.Alignment
import androidx.ui.core.Text
import androidx.ui.core.gesture.LongPressGestureDetector
import androidx.ui.core.setContent
import androidx.ui.foundation.DrawBackground
import androidx.ui.foundation.Icon
import androidx.ui.foundation.shape.RectangleShape
import androidx.ui.graphics.Color
import androidx.ui.graphics.RadialGradient

import androidx.ui.layout.*
import androidx.ui.material.*
import androidx.ui.material.icons.Icons
import androidx.ui.material.icons.rounded.ArrowBack
import androidx.ui.material.icons.rounded.ViewCompact
import androidx.ui.material.ripple.Ripple

import androidx.ui.text.TextStyle
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import androidx.work.*
import com.squareup.workflow.diagnostic.SimpleLoggingDiagnosticListener
import com.squareup.workflow.ui.ViewRegistry
import com.squareup.workflow.ui.WorkflowRunner
import com.squareup.workflow.ui.setContentWorkflow
import compose.bindCompose
import emot.BehaviorAndSkillsRepository
import emot.JournalRepository
import emot.RemindersRepository

import library.*
import org.konan.multiplatform.library.EntryWorkFlow
import sample.Database
import sample.RemindersTag
import sample.initLibrary
import sample.notificationsApi
import views.ComposeClock
import java.util.concurrent.TimeUnit
import screens.SkillsScreen


class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()


        initLibrary(this@MyApplication)
        val workManager = WorkManager.getInstance(applicationContext)
        notificationsApi.initializeNotifications()
        return
        /* RemindersRepository(Database).getReminders().onEach { list: List<Reminder> ->
             workManager.cancelAllWorkByTag(RemindersTag)
             val delays =
                 list.map {
                         getDelay(
                             *//*it.time.hours.toInt()*//*0,
                                *//*it.time.minutes.toInt() - it.time.hours.toInt() * 60*//*0
                            )
                        }
                        .map {
                            if (it.first > 0) it else it.copy(
                                first = DateTimeSpan(days = 1).totalMilliseconds
                                    .toLong() + it.first
                            )
                        }


                delays.forEachIndexed { i, it ->
                    DateTime.now().copyDayOfMonth()
                    val request = PeriodicWorkRequestBuilder<ShowReminderWorker>(
                        24, TimeUnit.HOURS
                    )
                        .setInitialDelay(it.first, TimeUnit.MILLISECONDS)
                        .addTag(RemindersTag)
                        .setInputData(
                            Data.Builder()
                                .putString("title", list[i].text)
                                .putInt("id", list[i].id!!.toInt()).build()
                        )
                        .build()

                    workManager.enqueueUniquePeriodicWork(
                        it.toString(),
                        ExistingPeriodicWorkPolicy.KEEP,
                        request
                    )
                }
            }
        */
    }

    private fun getDelay(hours: Int, minutes: Int): Pair<Long, Int> = TODO()
    /*DateTime.now().local.local.run {
        copyDayOfMonth(
            dayOfMonth = dayOfMonth, hours = hours,
            minutes = minutes,
            seconds = 0,
            milliseconds = 0
        ).let {
            it.unixMillisLong - unixMillisLong
        }
    } to hours*/

}

class ShowReminderWorker(appContext: Context, private val workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {
    override fun doWork(): Result {
        val map = workerParams.inputData.keyValueMap
        val text = map["title"].toString()
        val id = map["id"] as Int
        notificationsApi.createLocalNotification(
            "DBTime is now",
            "Напоминание: $text",
            id,
            RemindersTag
        )
        return Result.success()
    }
}


class MainActivity : AppCompatActivity() {
    private val viewRegistry = ViewRegistry(MainBinding())

    fun initTheme(lightTheme: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (lightTheme) {
                window.decorView.systemUiVisibility =
                    window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
            } else {
                window.decorView.systemUiVisibility =
                    window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            val color = if (lightTheme) -0x1000000 else 0x00000000
            window.statusBarColor = color
            window.navigationBarColor = color
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        initTheme(true)
        /*   val currentNightMode: Int = resources.configuration.uiMode and android.content.res.Configuration.UI_MODE_NIGHT_MASK
           when (currentNightMode) {
               android.content.res.Configuration.UI_MODE_NIGHT_NO -> true
               android.content.res.Configuration.UI_MODE_NIGHT_YES -> false
               else -> true
           }.let { lightTheme=it }*/


        super.onCreate(savedInstanceState)



        setContentWorkflow(viewRegistry) {

            val journalRepository = JournalRepository(Database)
            val behaviorAndSkillsRepository = BehaviorAndSkillsRepository(Database)
            val remindersRepository = RemindersRepository(Database)

            WorkflowRunner.Config(
                AppWorkflow(
                    entryWorkFlow = EntryWorkFlow(journalRepository, behaviorAndSkillsRepository),
                    journalWorkFlow = JournalWorkFlow(journalRepository),
                    remindersWorkFlow = RemindersWorkFlow(remindersRepository),
                    skillsWorkFlow = SkillsWorkFlow(behaviorAndSkillsRepository),
                    breatheWorkFlow = BreatheWorkFlow()
                ),
                diagnosticListener = SimpleLoggingDiagnosticListener()
            )
        }
    }

    override fun onBackPressed() {
        (state.value.screen.actionsLeft.firstOrNull() as? () -> Unit)?.invoke()
            ?: super.onBackPressed()
    }

    fun MainBinding() = bindCompose<AppWorkflow.Rendering> { rendering, _ ->
        state.value = rendering
        main()
    }

    @Preview("Main")
    @Composable
    fun mainPreview() {
        main()
    }

    @Model
    data class ContainerModel<T>(var value: T)

    val state = ContainerModel(
        AppWorkflow.Rendering(
            {},
            true,
            "Preview",
            AppWorkflow.ViewState.Graph,
            AppWorkflow.BottomBar({}, 1)
        )
    )

    @Composable
    fun main() {
        val rendering = state.value
        val lightTheme = state.value.lightTheme
        MaterialTheme(
            colors = if (lightTheme)
                darkColorPalette(
                    primary = Color(0xFF689F38),
                    primaryVariant = Color(0xFF3F9B3C),
                    secondary = Color(0xFFC8E6C9),
                    background = Color.Black,
                    surface = Color(0xE9121212),
                    error = Color(0xFFCF6679),
                    onPrimary = Color.Black,
                    onSecondary = Color.LightGray,
                    onBackground = Color.LightGray,
                    onSurface = Color.LightGray,
                    onError = Color.Black
                ) else
                lightColorPalette(
                    primary = Color(0xFF689F38),
                    primaryVariant = Color(0xFF3F9B3C)
                )
        ) {
            Surface(
                LayoutSize.Fill,
                //contentColor = MaterialTheme.colors().background,
                color = MaterialTheme.colors().background
            ) {
                DrawBackground(
                    shape = RectangleShape, brush = RadialGradient(
                        centerX = -200F,
                        centerY = -200F,
                        radius = 1000F,
                        colors = listOf(
                            Color(MaterialTheme.colors().background.value - 5000L),
                            MaterialTheme.colors().surface
                        )
                    )
                )
                Column {
                    Container(height = 70.dp, expanded = true) {
                        Stack(modifier = LayoutWidth.Fill +
                                    LayoutPadding(8.dp) +
                                    LayoutPadding(top = 16.dp) +
                                    LayoutHeight.Constrain(40.dp, 40.dp)
                        ) {
                            Row(modifier = LayoutSize.Fill) {
                                rendering.screen.actionsLeft.forEach {
                                    Container(LayoutHeight.Fill) {
                                        Button(
                                            modifier = LayoutAlign.CenterStart,
                                            elevation = 0.dp,
                                            onClick = (it as (() -> Unit)),
                                            backgroundColor = Color.Transparent,
                                            innerPadding = EdgeInsets(0.dp)
                                        ) {
                                            Icon(
                                                Icons.Rounded.ArrowBack,
                                                tint = MaterialTheme.colors().primary
                                            )
                                        }
                                    }
                                }

                                Container(modifier = LayoutHeight.Fill) {
                                    Ripple(bounded = false) {
                                        LongPressGestureDetector(onLongPress = {
                                            initTheme(lightTheme.not())
                                            rendering.onThemeToggle()
                                        }) {
                                            Text(
                                                text = rendering.header,
                                                style = TextStyle(color = MaterialTheme.colors().primary)
                                                //modifier = LayoutPadding(12.dp)
                                            )
                                        }
                                    }
                                }


                            }

                            when (val acts = rendering.screen) {
                                is AppWorkflow.ViewState.Journal -> Button(
                                    modifier = LayoutGravity.CenterEnd,
                                    backgroundColor = Color.Transparent,
                                    elevation = 4.dp,
                                    onClick = { acts.actions.first()() }) {
                                    Icon(
                                        Icons.Rounded.ViewCompact,
                                        tint = MaterialTheme.colors().primary
                                    )
                                }
                            }

                        }
                    }


                    Container(
                        alignment = Alignment.TopCenter,
                        modifier = LayoutWeight(1f),
                        expanded = true
                    ) {

                        when (val scr = rendering.screen) {
                            AppWorkflow.ViewState.EntryEditor -> Unit
                            AppWorkflow.ViewState.Graph -> Unit
                            is AppWorkflow.ViewState.Reminders -> RemindersScreen(scr)
                            is AppWorkflow.ViewState.Entry -> EntryScreen(scr)
                            is AppWorkflow.ViewState.Journal -> JournalScreen(scr)
                            is AppWorkflow.ViewState.Skills -> SkillsScreen(scr)
                            is AppWorkflow.ViewState.Breathe -> ComposeClock()
                        }.let { }

                    }


                    val data = rendering.bottomBar

                    TabRow(//color = MaterialTheme.colors().primaryVariant,
                        items = listOf("Навыки", "Журнал", "Напоминания", "Дыхание"),
                        indicatorContainer = { tabPositions ->
                            TabRow.IndicatorContainer(tabPositions, data.selected) {
                                TabRow.Indicator()
                            }
                        },
                        selectedIndex = data.selected
                    ) { position, name ->
                        Tab({ Text("$name") }, selected = data.selected == position, onSelected = {
                            data.onSelected(position)
                        })
                    }

                    Container(height = 48.5.dp, expanded = true) {
                        Stack(LayoutSize.Fill) {


                            DrawBackground(
                                shape = RectangleShape, brush = RadialGradient(
                                    centerX = 100F,
                                    centerY = 200F,
                                    radius = 600F,
                                    colors = listOf(
                                        if (lightTheme) MaterialTheme.colors().primary else MaterialTheme.colors().background,
                                        MaterialTheme.colors().surface
                                    )
                                )
                            )
                        }
                    }
                }
            }
        }
    }
}

package screens

import android.app.TimePickerDialog
import androidx.compose.*
import androidx.ui.core.ContextAmbient
import androidx.ui.core.Text
import androidx.ui.core.TextField
import androidx.ui.core.gesture.LongPressGestureDetector
import androidx.ui.foundation.Icon
import androidx.ui.foundation.ScrollerPosition
import androidx.ui.foundation.VerticalScroller
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.graphics.Color
import androidx.ui.layout.*
import androidx.ui.material.AlertDialog
import androidx.ui.material.Card
import androidx.ui.material.FloatingActionButton
import androidx.ui.material.MaterialTheme
import androidx.ui.material.icons.Icons
import androidx.ui.material.icons.outlined.Edit
import androidx.ui.material.icons.outlined.Save
import androidx.ui.material.icons.twotone.Add
import androidx.ui.text.TextFieldValue
import androidx.ui.unit.dp
import com.soywiz.klock.TimeSpan
import com.soywiz.klock.toTimeString
import library.AppWorkflow
import library.ReminderModel
import org.konan.multiplatform.views.PaddedCard


@Composable
fun RemindersScreen(scr: AppWorkflow.ViewState.Reminders) {

    Stack(modifier = LayoutSize.Fill) {

        val scroll = ScrollerPosition()
        if (scr.list.isEmpty()) {
            Center {
                Card(
                    shape = RoundedCornerShape(2.dp),
                    elevation = 2.dp
                ) {
                    Text(modifier = LayoutPadding(8.dp),text = "Тут пока пусто\nСоздайте новую запись")
                }
            }
        } else
            VerticalScroller(modifier = LayoutSize.Fill, scrollerPosition = scroll) {
                Column {
                    val showDialog = state {
                        false
                    }

                    scr.list.forEach { rem: ReminderModel ->
                        PaddedCard(modifier = LayoutWidth.Fill) {
                            LongPressGestureDetector(onLongPress = {

                                showDialog.value = true
                            }) {
                                Row(modifier = LayoutWidth.Fill, arrangement = Arrangement.SpaceBetween) {

                                    if (showDialog.value)
                                        AlertDialog(
                                                onCloseRequest = {
                                                    showDialog.value = false
                                                },
                                                title = {
                                                    Text(text = "Удаление")
                                                },
                                                text = {
                                                    Text("Удалить напоминание?\n\n${rem.reminder.text}\n${rem.reminder.time.toTimeString()}")
                                                },
                                                confirmButton = {
                                                    Button(onClick = {
                                                        rem.onDelete()
                                                    }) {
                                                        Text("Да")
                                                    }
                                                },
                                                dismissButton = {
                                                    Button(onClick = {
                                                        showDialog.value = false
                                                    }) {
                                                        Text("Отмена")
                                                    }
                                                }
                                        )
                                    if (rem.editing)
                                        TextField(modifier = LayoutSize.Constrain(40.dp, 200.dp), value = TextFieldValue(rem.reminder.text),
                                                onValueChange = {
                                                    rem.onEdit(rem.copy(reminder = rem.reminder.copy(text = it.text), editing = true))
                                                })
                                    else Text(text = rem.reminder.text, modifier = LayoutSize.Constrain(40.dp, 200.dp))


                                    Row(arrangement = Arrangement.SpaceBetween, modifier = LayoutWidth(150.dp)) {
                                        val context = ContextAmbient.current
                                        Button(
                                            backgroundColor = Color.Transparent,
                                            onClick = {
                                            if (rem.editing)
                                                TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                                                    rem.onEdit(rem.copy(editing = true, reminder = rem.reminder.copy(time = TimeSpan(hourOfDay * 60 * 60 * 1000.0 + minute * 60 * 1000.0))))
                                                }, /*rem.reminder.time.hours.toInt()*/0,/* (rem.reminder.time - TimeSpan(rem.reminder.time.hours.toInt() * 60 * 60 * 1000.0)).minutes.toInt()*/0, true).show()
                                        }) {
                                            Text(rem.reminder.time.toTimeString())
                                        }


                                        Button(
                                            backgroundColor = Color.Transparent,
                                            onClick = {
                                            rem.onEdit(rem.copy(editing = rem.editing.not()))
                                            if (rem.editing) {
                                                rem.onSave(rem)
                                            }
                                        }) {
                                            Icon(if (rem.editing) {
                                                Icons.Outlined.Save
                                            } else {
                                                Icons.Outlined.Edit
                                            }, tint = MaterialTheme.colors().primary)
                                        }

                                    }
                                }
                            }
                        }
                    }
                    Spacer(modifier = LayoutHeight(12.dp))
                }

            }

        
        

        FloatingActionButton(color = MaterialTheme.colors().surface, elevation = 8.dp,modifier = LayoutPadding(8.dp) + LayoutGravity.BottomEnd, onClick = {
            scr.onNewEntry()
        }) {
            Icon(Icons.TwoTone.Add,tint = MaterialTheme.colors().primary)
        }

    }
}
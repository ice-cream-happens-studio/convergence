package screens

import androidx.compose.*
import androidx.ui.core.*
import androidx.ui.foundation.Icon
import androidx.ui.foundation.VerticalScroller
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.geometry.Offset
import androidx.ui.graphics.Canvas
import androidx.ui.graphics.Color
import androidx.ui.graphics.Paint


import androidx.ui.layout.*
import androidx.ui.material.Card
import androidx.ui.material.FloatingActionButton
import androidx.ui.material.MaterialTheme
import androidx.ui.material.icons.Icons
import androidx.ui.material.icons.rounded.Edit
import androidx.ui.material.icons.twotone.Add
import androidx.ui.text.AnnotatedString
import androidx.ui.text.SpanStyle
import androidx.ui.text.font.FontWeight
import androidx.ui.unit.PxSize
import androidx.ui.unit.dp
import androidx.ui.unit.px
import com.soywiz.klock.weekOfYear1
import emot.Behavior
import emot.JournalEntryData
import emot.SkillEntry
import library.AppWorkflow
import org.konan.multiplatform.views.PaddedCard
import kotlin.math.roundToInt
import androidx.ui.unit.*

@Composable
fun JournalScreen(scr: AppWorkflow.ViewState.Journal) {
    Stack(modifier = LayoutSize.Fill) {

        if (scr.list.isEmpty()) {
            Center {
                Card(
                    shape = RoundedCornerShape(2.dp),
                    elevation = 2.dp
                ) {
                    Text(
                        modifier = LayoutPadding(8.dp),
                        text = "Тут пока пусто\nСоздайте новую запись"
                    )
                }
            }
        } else
            VerticalScroller {
                Column {
                    scr.list.forEachIndexed { ind, entry ->
                        val previousEntry = scr.list.getOrNull(ind - 1)
                        if (previousEntry?.first?.dayOfYear != entry.first.dayOfYear) {
                            StraightLine()

                            Row(
                                arrangement = Arrangement.SpaceBetween,
                                modifier = LayoutPadding(4.dp) + LayoutSize.Fill
                            ) {
                                Text(entry.first.format("День ${entry.first.dayOfMonth}"))
                                if (previousEntry?.first?.weekOfYear1 != entry.first.weekOfYear1) {
                                    Text(text = "Неделя ${entry.first.weekOfYear1}")
                                }
                            }
                        }



                        PaddedCard {
                            Column(modifier = LayoutAlign.Center) {

                                Row(
                                    arrangement = Arrangement.SpaceBetween,
                                    modifier = LayoutWidth.Fill
                                ) {
                                    Text(entry.first.format("HH:mm:ss"))

                                    Button(
                                        backgroundColor = Color.Transparent,
                                        modifier = LayoutAlign.CenterEnd,
                                        onClick = {
                                            scr.onClick(entry.first)
                                        }) {

                                        Icon(
                                            Icons.Rounded.Edit,
                                            tint = MaterialTheme.colors().primary
                                        )
                                    }

                                }

                                GeneralDataViewCompact(entry.second.entry)
                                entry.second.behavior.forEach { BehaviorViewCompact(it) }

                                (0..entry.second.skills.lastIndex step 2).forEach { i ->
                                    Row(arrangement = Arrangement.SpaceEvenly) {
                                        Container(LayoutWeight(0.5f)) {
                                            entry.second.skills.getOrNull(i)
                                                ?.let { SkillCardCompact(it) }
                                        }
                                        Container(LayoutWeight(0.5f)) {
                                            entry.second.skills.getOrNull(i + 1)
                                                ?.let { SkillCardCompact(it) }
                                        }
                                    }
                                }
                            }
                        }
                        Spacer(modifier = LayoutHeight(60.dp))
                    }

                }
            }
        FloatingActionButton(
            color = MaterialTheme.colors().surface,
            elevation = 8.dp,
            modifier = LayoutPadding(8.dp) + LayoutGravity.BottomEnd,
            onClick = {
                scr.onNewEntry()
            }) {
            Icon(Icons.TwoTone.Add, tint = MaterialTheme.colors().primary)
        }
    }
}

@Composable
private fun StraightLine() {
    val paint = Paint().apply {
        color = MaterialTheme.colors().primary
    }
    Draw(onPaint = { canvas: Canvas, parentSize: PxSize ->
        canvas.drawLine(
            Offset.zero,
            Offset(parentSize.width.value, 0F),
            paint.apply { strokeWidth = (4.dp.toPx().value) }
        )
    })
}


@Composable
fun BehaviorViewCompact(b: Behavior) {
    PaddedCard {
        Column() {


            Row(LayoutWidth.Fill, Arrangement.SpaceAround) {
                Text(text = b.name)
            }
            Row(LayoutWidth.Fill, Arrangement.SpaceBetween) {
                Text(
                    text = AnnotatedString("Действие: ") + AnnotatedString(
                        "${(b.actionLevel * 7).roundToInt()}",
                        SpanStyle(fontWeight = FontWeight.Bold)
                    )
                )
                Text(
                    text = AnnotatedString("Желание: ") + AnnotatedString(
                        "${(b.desireLevel * 7).roundToInt()}",
                        SpanStyle(fontWeight = FontWeight.Bold)
                    )
                )
            }

        }
    }
}

@Composable
fun SkillCardCompact(s: SkillEntry) {
    PaddedCard {
        Row {
            Text(
                modifier = LayoutWidth.Fill,
                text = AnnotatedString("${s.name}: ") + AnnotatedString(
                    "${(s.score * 7).roundToInt()}",
                    SpanStyle(fontWeight = FontWeight.Bold)
                )
            )
        }
    }
}

@Composable
fun GeneralDataViewCompact(d: JournalEntryData) {
    PaddedCard {
        Column(LayoutWidth.Fill) {
            Center {
                Text(text = "Боль")
            }
            Row(LayoutWidth.Fill, arrangement = Arrangement.SpaceBetween) {
                Text(
                    AnnotatedString("Эмоциональная: ") + AnnotatedString(
                        "${(d.emotionalDiscomfort * 7).roundToInt()}",
                        SpanStyle(fontWeight = FontWeight.Bold)
                    )
                )
                Text(
                    AnnotatedString("Физическая: ") + AnnotatedString(
                        "${(d.physicalDiscomfort * 7).roundToInt()}",
                        SpanStyle(fontWeight = FontWeight.Bold)
                    )
                )
            }
            Text(
                AnnotatedString("Радость: ") + AnnotatedString(
                    "${(d.happiness * 7).roundToInt()}",
                    SpanStyle(fontWeight = FontWeight.Bold)
                )
            )
            Text(
                AnnotatedString("Прекращение терапии: ") + AnnotatedString(
                    "${(d.therapyDiscontinuation * 7).roundToInt()}",
                    SpanStyle(fontWeight = FontWeight.Bold)
                )
            )
        }
    }
}
package screens

import androidx.compose.*
import androidx.ui.core.Modifier
import androidx.ui.core.Text
import androidx.ui.foundation.Border
import androidx.ui.foundation.ScrollerPosition
import androidx.ui.foundation.VerticalScroller
import androidx.ui.graphics.Color
import androidx.ui.graphics.Shape
import androidx.ui.layout.*

import androidx.ui.material.MaterialTheme
import androidx.ui.material.contentColorFor

import androidx.ui.unit.Dp
import androidx.ui.unit.dp
import library.AppWorkflow

import org.konan.multiplatform.views.GeneralDataView
import emot.Behavior
import emot.JournalEntryData
import emot.SkillEntry
import views.BehaviorView
import views.SliderCard


private val ButtonHorizontalPadding = 16.dp
private val ButtonVerticalPadding = 8.dp

private val SmallButtonHorizontalPadding = 8.dp
private val SmallButtonVerticalPadding = 4.dp

@PublishedApi
internal val ButtonPaddings = EdgeInsets(
    left = ButtonHorizontalPadding,
    top = ButtonVerticalPadding,
    right = ButtonHorizontalPadding,
    bottom = ButtonVerticalPadding
)

@PublishedApi
internal val SmallButtonPaddings = EdgeInsets(
    left = SmallButtonHorizontalPadding,
    top = SmallButtonVerticalPadding,
    right = SmallButtonHorizontalPadding,
    bottom = SmallButtonVerticalPadding
)


@Composable
fun Button(
    onClick: () -> Unit,
    modifier: Modifier = Modifier.None,
    enabled: Boolean = true,
    elevation: Dp = 2.dp,
    shape: Shape = MaterialTheme.shapes().button,
    border: Border? = null,
    backgroundColor: Color = MaterialTheme.colors().primary,
    contentColor: Color = contentColorFor(backgroundColor),
    innerPadding: EdgeInsets = androidx.ui.material.Button.DefaultInnerPadding,
    children: @Composable() () -> Unit
)
 = androidx.ui.material.Button(
    onClick,
        modifier, enabled, elevation, shape, border,backgroundColor,contentColor,innerPadding, children
)

@Composable
fun EntryScreen(scr: AppWorkflow.ViewState.Entry) {
    Stack(modifier = LayoutSize.Fill) {
        val scroll =  ScrollerPosition()
        VerticalScroller(modifier = LayoutSize.Fill, scrollerPosition = scroll) {

            Column {
                scr.list.forEach {
                    when (it) {
                        is Behavior -> BehaviorView(it, scr.onEntryEdited)
                        is SkillEntry -> SliderCard(it, scr.onEntryEdited)
                        is JournalEntryData -> GeneralDataView(it, scr.onEntryEdited)
                    }.let { }
                }
                Spacer(modifier = LayoutHeight(256.dp))
                onCommit(scr.page, callback = {
                    scroll.scrollTo(0F)
                })
            }

        }


        if (scr.page != 0)
        TextButton(modifier = LayoutPadding(8.dp) + LayoutGravity.BottomStart, onClick = {
                scr.onPreviousPage()
        }) {
                Text("Назад")
        }

        TextButton(modifier = LayoutPadding(8.dp) + LayoutGravity.BottomEnd, onClick = {
            if (scr.page == 2)
                scr.onSave()
            else
                scr.onNextPage()
        }) {
            if (scr.page == 2)
                Text("Сохранить")
            else
                Text("Далее")
        }

    }
}

@Composable
fun TextButton(
        modifier: Modifier = Modifier.None,
        onClick: () -> Unit,
        backgroundColor: Color = Color(0x20888888),
        contentColor: Color = MaterialTheme.colors().primary,
        shape: Shape = MaterialTheme.shapes().button,
        border: Border? = null,
        elevation: Dp = 0.dp,
        //paddings: EdgeInsets = EdgeInsets(TextButtonHorizontalPadding, TextButtonHorizontalPadding),
         children: @Composable() () -> Unit
) = Button(
        modifier = modifier,
        onClick = onClick,
        backgroundColor = backgroundColor,
        contentColor = contentColor,
        shape = shape,
        border = border,
        elevation = elevation,
        //paddings = paddings,
        children = children
)

val TextButtonHorizontalPadding = 8.dp

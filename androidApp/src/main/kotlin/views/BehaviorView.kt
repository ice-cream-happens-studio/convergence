package views

import androidx.compose.Composable
import androidx.ui.core.*
import androidx.ui.layout.*
import androidx.ui.material.MaterialTheme
import androidx.ui.material.Slider
import androidx.ui.material.SliderPosition
import androidx.ui.text.TextStyle
import androidx.ui.unit.TextUnit
import androidx.ui.unit.dp
import emot.*
import org.konan.multiplatform.views.PaddedCard


@Composable
fun BehaviorView(entry: TwoPointEntry, onEntryEdited: (Entry) -> Unit) =
        PaddedCard {
            Column {
                TwoSlidersCard(entry, onEntryEdited)
            }
        }


@Composable
fun TwoSlidersCard(entry: TwoPointEntry, onEntryEdited: (Entry) -> Unit) {
    Row(LayoutPadding(8.dp)) {

            Text(text = entry.name())

    }
    Row {
        val onSlided = when (entry) {
            is Behavior -> {
                (entry.desireLevel to entry.actionLevel) to
                        ({ it: Float -> onEntryEdited(entry.copy(desireLevel = it)) } to { it: Float -> onEntryEdited(entry.copy(actionLevel = it)) })
            }
            is JournalEntryData -> {
                (entry.emotionalDiscomfort to entry.physicalDiscomfort) to
                        ({ it: Float -> onEntryEdited(entry.copy(emotionalDiscomfort = it)) } to { it: Float -> onEntryEdited(entry.copy(physicalDiscomfort = it)) })
            }
        }

        Column(LayoutWeight(0.5F)) {
            val pos = SliderPosition(steps = 5, initial = onSlided.first.first)
            Slider(position = pos, onValueChangeEnd = { onSlided.second.first(pos.value) })
            Text(text = entry.subtitle().first, modifier = LayoutPadding(4.dp),
                    style = TextStyle(
                            color = MaterialTheme.colors().onSurface,
                            fontSize = TextUnit.Sp(10)
                    ))
        }
        Column(LayoutWeight(0.5F)) {
            val pos = SliderPosition(steps = 5, initial = onSlided.first.second)
            Slider(position = pos, onValueChangeEnd = { onSlided.second.second(pos.value) })
            Text(text = entry.subtitle().second, modifier = LayoutPadding(4.dp),
                    style = TextStyle(color = MaterialTheme.colors().onSurface,
                            fontSize = TextUnit.Sp(10)))
        }
    }
}

@Composable
fun SliderCard(skill: SkillEntry, onEntryEdited: (Entry) -> Unit) = PaddedCard {
    Column {

            Row(LayoutPadding(8.dp)) {
                Text(text = skill.name)
            }

        Row {
            Column {
                val pos = SliderPosition(steps = 5, initial = skill.score)
                Slider(position = pos, onValueChangeEnd = { onEntryEdited(skill.copy(score = pos.value)) })

                    Text(modifier = LayoutPadding(4.dp),text = skill.subtitle(), style = TextStyle(
                            color = MaterialTheme.colors().onSurface,
                            fontSize = TextUnit.Sp(10)
                    ))

            }
        }
    }
}

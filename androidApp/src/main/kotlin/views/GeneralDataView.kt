package org.konan.multiplatform.views

import androidx.compose.Composable
import androidx.compose.remember
import androidx.compose.state
import androidx.ui.core.LayoutModifier
import androidx.ui.core.Modifier
import androidx.ui.core.Text
import androidx.ui.core.TextField
import androidx.ui.foundation.Border
import androidx.ui.foundation.ScrollerPosition
import androidx.ui.foundation.shape.corner.RoundedCornerShape
import androidx.ui.layout.*
import androidx.ui.material.Card
import androidx.ui.material.Slider
import androidx.ui.material.SliderPosition

import androidx.ui.unit.dp
import androidx.ui.unit.px
import emot.Entry
import emot.JournalEntryData

import views.TwoSlidersCard
import kotlin.math.roundToInt


@Composable
fun GeneralDataView(entry: JournalEntryData, onEntryEdited: (Entry) -> Unit) = Column {
    PaddedCard {
        Column {
            TwoSlidersCard(entry, onEntryEdited)
        }
    }

    Spacer(modifier = LayoutHeight(4.dp))
    PaddedCard {
        Column() {
            Row {
                Text(text = "Радость")
            }
            Row {
                val pos = SliderPosition(steps = 5, initial = entry.happiness)
                Slider(position = pos, onValueChangeEnd = {
                    onEntryEdited(entry.copy(happiness = pos.value))
                })
            }
        }
    }

    Spacer(modifier = LayoutHeight(4.dp))
    PaddedCard {
        Column {
            Row {
                Text(text = "Желание прекратить терапию")
            }
            Row {
                val pos = SliderPosition(steps = 5, initial = entry.therapyDiscontinuation)
                Slider(position = pos, onValueChangeEnd = {
                    onEntryEdited(entry.copy(therapyDiscontinuation = pos.value))
                })
            }
        }
    }
}


@Composable
fun PaddedCard(modifier:Modifier = Modifier.None, children: @Composable() () -> Unit) {
    Card(
            shape = RoundedCornerShape(8.dp),
            elevation = 2.dp,
            modifier = LayoutPadding(4.dp)+modifier
    ) {
        Container(LayoutPadding(6.dp)) {
            children()
        }
    }
}

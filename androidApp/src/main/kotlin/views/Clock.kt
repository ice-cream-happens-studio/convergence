package views

import androidx.animation.*
import androidx.compose.*
import androidx.ui.animation.Transition
import androidx.ui.core.Draw
import androidx.ui.geometry.Offset
import androidx.ui.geometry.Rect
import androidx.ui.graphics.Canvas
import androidx.ui.graphics.Color
import androidx.ui.graphics.Paint
import androidx.ui.graphics.PaintingStyle
import androidx.ui.layout.LayoutPadding
import androidx.ui.layout.LayoutSize
import androidx.ui.layout.Stack
import androidx.ui.unit.*
import java.util.*
import kotlin.math.*

/**
 * @author Adib Faramarzi (adibfara@gmail.com)
 */



var Duration = 7000
@Composable
fun ComposeClock() {
    Stack(modifier = LayoutSize.Fill) {
        val clockConfig = ClockConfig(
            Random()
        )

        //ClockBackground(clockConfig)

        Stack(LayoutSize.Fill+ LayoutPadding(8.dp)) {

            /**
             * Background particles
             */

            /**
             * Background particles
             */

            repeat(1000) { value ->
                ParticleHeartBeat(
                    clockConfig,
                    ParticleObject.Type.Background
                )
            }


/*            */
            /**
             * Hour handle
             *//*

            */
            /**
             * Hour handle
             *//*j
            repeat(100) {
                ParticleHeartBeat(
                    clockConfig,
                    ParticleObject.Type.Hour
                )
            }

            */
            /**
             * Minute handle
             *//*

            */
            /**
             * Minute handle
             *//*
            repeat(100) {
                ParticleHeartBeat(
                    clockConfig,
                    ParticleObject.Type.Minute
                )
            }*/

            /*ClockBackgroundBorder(clockConfig)
            ClockMinuteCircles(clockConfig)*/
            ClockSecondHand(clockConfig)
        }
    }
}

@Composable
private fun ClockBackground(clockConfig: ClockConfig) {
    val backgroundPaint = Paint().apply {
        color = clockConfig.colorPalette.backgroundColor
        style = PaintingStyle.fill
    }
    Draw(onPaint = { canvas: Canvas, parentSize: PxSize ->
        canvas.drawRect(parentSize.toRect(), backgroundPaint)
    })
}

@Composable
private fun ClockSecondHand(clockConfig: ClockConfig) {


    /*AnimationClockAmbient.current.subscribe(object : AnimationClockObserver {
        var changed = false
        var startOffset:Long?=null
        override fun onAnimationFrame(frameTimeMillis: Long) {
            val startOffset = if(startOffset==null) {
                startOffset = frameTimeMillis
                frameTimeMillis
            } else startOffset!!

            if((frameTimeMillis-startOffset) % 7000 < 50) {
                if(changed.not()) {
                    direction.value=direction.value.not()
                }
                changed = true
            }
            if((frameTimeMillis-startOffset) % 7000 > 6950) {
                changed = false
            }
        }
    })*/
    var direction = true
    var changed = true
    Transition(definition = SecondHandAnimations, initState = 0, toState = 1) { state ->
        Draw(onPaint = { canvas, parentSize ->

            if(changed.not() && state[SecondHandProgress]<0.05F) {
                direction=!direction
                changed=true
            }
            if(changed && state[SecondHandProgress]>0.95f) {
                changed=false
            }

            val paint = Paint().apply {
                style = PaintingStyle.fill
                color = clockConfig.colorPalette.handleColor
            }
            paint.style = PaintingStyle.fill



            val strokeWidth = 4.dp.toPx().value
            paint.style = PaintingStyle.stroke
            paint.strokeWidth = strokeWidth
            paint.isAntiAlias = true

            val innerRadius = (parentSize.minDimension.value - strokeWidth) / 2
            val parentHalfWidth = parentSize.width.value / 2
            val parentHalfHeight = parentSize.height.value / 2
            val rect = Rect(
                parentHalfWidth - innerRadius,
                parentHalfHeight - innerRadius,
                parentHalfWidth + innerRadius,
                parentHalfHeight + innerRadius
            )
            val startAngle = 90F
            val sweep = 180*(if (direction) state[SecondHandProgress] else (1-state[SecondHandProgress]))
            canvas.drawArc(rect, startAngle, sweep, false, paint = paint)
            canvas.drawArc(rect, startAngle, -sweep, false, paint = paint)
        })
    }
}

@Composable
private fun ClockMinuteCircles(clockConfig: ClockConfig) {
    Draw(onPaint = { canvas: Canvas, parentSize: PxSize ->
        val clockRadius = 0.95f * min((parentSize.width / 2), (parentSize.height / 2)).value
        val paint = Paint().apply {
            style = PaintingStyle.fill
            color = clockConfig.colorPalette.handleColor
        }
        val centerX = (parentSize.width / 2).value
        val centerY = (parentSize.height / 2).value
        val oneMinuteRadians = Math.PI / 30
        0.rangeTo(59).forEach { minute ->
            val isHour = minute % 5 == 0
            val degree = -Math.PI / 2 + (minute * oneMinuteRadians)
            val x = centerX + cos(degree) * clockRadius
            val y = centerY + sin(degree) * clockRadius

            val radius: Float
            if (isHour) {
                paint.style = PaintingStyle.fill
                radius = 12f
            } else {
                paint.style = PaintingStyle.stroke
                radius = 6f
            }
            canvas.drawCircle(
                (Offset(x.toFloat(), y.toFloat())),
                radius,
                paint
            )
        }

    })
}

@Composable
private fun ClockBackgroundBorder(clockConfig: ClockConfig) {
    Draw(onPaint = { canvas: Canvas, parentSize: PxSize ->
        val radius = min((parentSize.width / 2), (parentSize.height / 2)).value * 0.9f
        val paint = Paint().apply {
            style = PaintingStyle.stroke
            strokeWidth = 10f
            color = clockConfig.colorPalette.borderColor
        }
        canvas.drawCircle(
            (Offset((parentSize.width / 2).value, (parentSize.height / 2).value)),
            radius,
            paint
        )
    })
}


@Composable
fun ParticleHeartBeat(
    clockConfig: ClockConfig,
    type: ParticleObject.Type
) {
    val paint = Paint()
    val random = Random()
    var particle: ParticleObject? = null


    var direction = 1f
    /*AnimationClockAmbient.current.subscribe(object : AnimationClockObserver {
        var changed = false
        var startOffset:Long?=null
        override fun onAnimationFrame(frameTimeMillis: Long) {
            val startOffset = if(startOffset==null) {
                startOffset = frameTimeMillis
                frameTimeMillis
            } else startOffset!!

            if((frameTimeMillis-startOffset) % 7000 < 100) {
                if(changed.not()) {
                    direction.value=-direction.value
                }
                changed = true
            }
            if((frameTimeMillis-startOffset) % 7000 > 6950) {
                changed = false
            }
        }
    })
*/
    var changed = false
    var direct = -1
    Transition(definition = ParticleAnimations, initState = 0, toState = 1,onStateChangeFinished =
    {

    }) { state ->
        Draw(onPaint = { canvas, size ->
            val prog = state[ParticleProgress]
           /* if(changed.not() && prog <0.05F) {
                direct=-direct
                direction = 1F*direct
                changed = true
            }

            direction = if(prog <0.30F) {
                direct.toFloat()*((prog+0.02F)*3F)
            } else if(prog>0.9F) {
                (0.9+1-prog).toFloat()
            } else {
                direct.toFloat()
            }


            if(changed && prog >0.95F) {
                changed = false
            }*/
            val progress = 1 - prog

            if (particle == null) {
                particle = ParticleObject(type, clockConfig).also {
                    it.randomize(random, size)
                }
            }
            particle!!.apply {
                animate(
                    direction,
                    progress,
                    size
                )
                drawOnCanvas(paint, canvas)
            }

        })
    }
}


private fun ParticleObject.animate(
    direction: Float,
    progress: Float,
    size: PxSize
) {
    val centerX = size.width / 2
    val centerY = size.height / 2
    val radius = min(centerX, centerY).value
    val random = Random()
    val modifier = max(0.2f, progress) * 1.79f//* randomFloat(1f, 4f, random)
    val xUpdate = modifier * cos(animationParams.currentAngle)
    val yUpdate = modifier * sin(animationParams.currentAngle)
    val newX = animationParams.locationX.value + xUpdate*direction
    val newY = animationParams.locationY.value + yUpdate*direction

    val positionInsideCircle =
        hypot(newY - centerY.value, newX - centerX.value)
    val currentPositionIsInsideCircle = positionInsideCircle < radius * type.maxLengthModifier
    val currentLengthByRadius = positionInsideCircle / (radius * type.maxLengthModifier)
    when {
        currentLengthByRadius - type.minLengthModifier <= 0f -> {
            //animationParams.alpha = 0f
        }
        animationParams.alpha == 0f -> {
            animationParams.alpha = random.nextFloat()

        }
        else -> {
            val fadeOutRange = this.type.maxLengthModifier+0.1F
            animationParams.alpha =
                (if (currentLengthByRadius < fadeOutRange) animationParams.alpha else ((1f - currentLengthByRadius) / (1f - fadeOutRange))).coerceIn(
                    0f,
                    1f
                )
        }
    }
    if (!currentPositionIsInsideCircle) {
        if(progress>0.95F)
            randomize(random, size)
        animationParams.alpha = 0f
    }
        animationParams.locationX = Dp(newX)
        animationParams.locationY = Dp(newY)



}

private fun ParticleObject.drawOnCanvas(paint: Paint, canvas: Canvas) {
    canvas.apply {
        paint.color = animationParams.currentColor
        paint.alpha = animationParams.alpha
        val centerW = animationParams.locationX.value
        val centerH = animationParams.locationY.value
        if (animationParams.isFilled) {
            paint.style = PaintingStyle.fill
        } else {
            paint.style = PaintingStyle.stroke

        }
        drawCircle(
            Offset(centerW, centerH),
            animationParams.particleSize.value / 2f,
            paint
        )
    }
}


private fun ParticleObject.randomize(
    random: Random,
    pxSize: PxSize
) {
    val randomAngleOffset =
        randomFloat(type.startAngleOffsetRadians, type.endAngleOffsetRadians, random)
    val randomizedAngle = when (type) {
        ParticleObject.Type.Hour -> TODO()
        ParticleObject.Type.Minute -> TODO()
        ParticleObject.Type.Background -> randomAngleOffset
    }
    val centerX = (pxSize.width / 2).value + randomFloat(-10f, 10f, random)
    val centerY = (pxSize.height / 2).value + randomFloat(-10f, 10f, random)

    val radius = kotlin.math.min(centerX, centerY)
    val randomLength =
        randomFloat(
            type.minLengthModifier * radius,
            type.maxLengthModifier / 1.5F * radius,
            random
        )

    val x = randomLength * cos(randomizedAngle)
    val y = randomLength * sin(randomizedAngle)
    val color = when (type) {
        ParticleObject.Type.Background -> clockConfig.colorPalette.mainColors.random()
        ParticleObject.Type.Hour -> clockConfig.colorPalette.handleColor
        ParticleObject.Type.Minute -> clockConfig.colorPalette.handleColor
    }
    animationParams = ParticleObject.AnimationParams(
        isFilled = clockConfig.random.nextFloat() < 0.7f,
        alpha = (random.nextFloat()).coerceAtLeast(0f),
        locationX = Dp(centerX + x),
        locationY = Dp(centerY + y),
        particleSize = Dp(randomFloat(type.minSize.value, type.maxSize.value, random)),
        currentAngle = randomizedAngle.toFloat(),
        progressModifier = randomFloat(1f, 2f, random),
        currentColor = color
    )
}


val ParticleProgress = FloatPropKey()


val ParticleAnimations = transitionDefinition {
    state(0) {
        this[ParticleProgress] = 0f
    }

    state(1) {
        this[ParticleProgress] = 1f
    }

    transition(fromState = 0, toState = 1) {
        ParticleProgress using repeatable<Float> {
            iterations = Infinite

            animation = tween {
                easing = FastOutLinearInEasing
                duration = Duration
            }
        }
    }
}

val SecondHandProgress = FloatPropKey()

val SecondHandAnimations = transitionDefinition {
    state(0) {
        this[SecondHandProgress] = 0f
    }

    state(1) {
        this[SecondHandProgress] = 1f
    }

    transition(fromState = 0, toState = 1) {
        SecondHandProgress using repeatable<Float> {
            iterations = Infinite
            animation = tween {
                duration = Duration
            }
        }
    }
}


data class ClockConfig(
    val random: Random,
    val colorPalette: ColorPalette = ColorPalette.Adrift,
    val maxCount: Int = 100,
    val hourCount: Int = 50,
    val minuteCount: Int = 100
)

enum class ColorPalette(
    val mainColors: Array<Color>,
    val handleColor: Color,
    val dividerColor: Color,
    val borderColor: Color,
    val backgroundColor: Color

) {
    Adrift(
        arrayOf(
            Color(0xFF99B898),
            Color(0xFFFECEA8),
            Color(0xFFFF847C)
        ),
        Color(0xFF62BE5B),
        Color(0x39E84A5F),
        Color(0x41E84A5F),
        Color(0xff2A363B)
    ),
}

data class ParticleObject(
    val type: Type,
    val clockConfig: ClockConfig,
    var animationParams: AnimationParams = AnimationParams()
) {
    data class AnimationParams(
        var locationX: Dp = Dp(-1f),
        var locationY: Dp = Dp(-1f),
        var alpha: Float = -1f,
        var isFilled: Boolean = false,
        var currentColor: Color = Color(0),
        var particleSize: Dp = Dp(0f),
        var currentAngle: Float = 1f,
        var progressModifier: Float = 1f
    )

    enum class Type(
        val startAngleOffsetRadians: Float,
        val endAngleOffsetRadians: Float,
        val maxLengthModifier: Float,
        val minLengthModifier: Float,
        val minSize: Dp,
        val maxSize: Dp
    ) {
        Background(
            0f,
            3F * Math.PI.toFloat(),
            0.85f,
            0.15f,
            Dp(4f),
            Dp(12f)
        ),
        Hour(
            0f,
            0f,
            0.6f,
            0.01f,
            Dp(8f),
            Dp(32f)
        ),
        Minute(
            0f,
            0f,
            0.75f,
            0.01f,
            Dp(8f),
            Dp(32f)
        )
    }
}

fun randomFloat(start: Float, end: Float, random: Random): Float {
    return start + random.nextFloat() * (end.toInt() - start.toInt())
}


private const val DividerLengthInDegrees = 1.8f
private val AngleOffset = FloatPropKey()
private val Shift = FloatPropKey()

private val CircularTransition = transitionDefinition {
    state(0) {
        this[AngleOffset] = 0f
        this[Shift] = 0f
    }
    state(1) {
        this[AngleOffset] = 360f
        this[Shift] = 30f
    }
    transition(fromState = 0, toState = 1) {
        AngleOffset using tween<Float> {
            delay = 500
            duration = 900
            easing = CubicBezierEasing(0f, 0.75f, 0.35f, 0.85f)
        }
        Shift using tween<Float> {
            delay = 500
            duration = 900
            easing = LinearOutSlowInEasing
        }
    }
}

/** when calculating a proportion of N elements, the sum of elements has to be (1 - N * 0.005)
 * because there will be N dividers of size 1.8 degrees */
@Composable
fun DrawAnimatedCircle(proportions: List<Float>, colors: List<Color>) {
    val strokeWidthDp = 5.dp
    val paint = remember { Paint() }
    Transition(definition = CircularTransition, toState = 1) { state ->
        Draw { canvas, parentSize ->
            val strokeWidth = strokeWidthDp.toPx().value
            paint.style = PaintingStyle.stroke
            paint.strokeWidth = strokeWidth
            paint.isAntiAlias = true

            val innerRadius = (parentSize.minDimension.value - strokeWidth) / 2
            val parentHalfWidth = parentSize.width.value / 2
            val parentHalfHeight = parentSize.height.value / 2
            val rect = Rect(
                parentHalfWidth - innerRadius,
                parentHalfHeight - innerRadius,
                parentHalfWidth + innerRadius,
                parentHalfHeight + innerRadius
            )
            var startAngle = state[Shift] - 90f
            proportions.forEachIndexed { index, proportion ->
                paint.color = colors[index]
                val sweep = proportion * state[AngleOffset]
                canvas.drawArc(rect, startAngle, sweep, false, paint = paint)
                startAngle += sweep + DividerLengthInDegrees
            }
        }
    }
}